package id.asteris.testandroid;

import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import id.asteris.testandroid.adapter.CommentAdapter;
import id.asteris.testandroid.connection.GetComments;
import id.asteris.testandroid.model.Comment;
import id.asteris.testandroid.model.Story;

public class FullStoryActivity extends AppCompatActivity {

    public static Story story;

    private TextView title;
    private TextView by;
    private TextView date;
    private WebView description;
    private ListView comments;
    private ImageView star;

    public static boolean isClicked = false;

    private ArrayList<Comment> commentArrayList;
    private CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_story);

        title = findViewById(R.id.aFullStory_title);
        by = findViewById(R.id.aFullStory_by);
        date = findViewById(R.id.aFullStory_date);
        description = findViewById(R.id.aFullStory_description);
        comments = findViewById(R.id.aFullStory_comments);
        star = findViewById(R.id.aFullStory_star);

        title.setText(story.getTitle());
        by.setText(story.getBy());
        date.setText(story.getDate());

        description.loadUrl(story.getUrl());
        description.setWebViewClient(new WebViewClient());

        commentArrayList = new ArrayList<>();

        adapter = new CommentAdapter(this, commentArrayList);
        comments.setAdapter(adapter);

        getCommentsNow();

        initViewClicked();
    }

    private void initViewClicked() {
        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isClicked) {
                    isClicked = false;
                    star.setColorFilter(ContextCompat.getColor(v.getContext(), R.color.colorStarGrey), PorterDuff.Mode.SRC_IN);
                } else {
                    isClicked = true;
                    star.setColorFilter(ContextCompat.getColor(v.getContext(), R.color.colorStarClicked), PorterDuff.Mode.SRC_IN);
                }
            }
        });
    }

    private void getCommentsNow() {
        for(int i=0;i<story.getComments().length;i++) {
            int idComment = story.getComments()[i];
            GetComments getComments = new GetComments(this, idComment);
            getComments.execute();
        }
    }

    public void addComment(Comment comment) {
        commentArrayList.add(comment);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isClicked) {
            MainActivity.favTitle = story.getTitle();
        }
    }
}
