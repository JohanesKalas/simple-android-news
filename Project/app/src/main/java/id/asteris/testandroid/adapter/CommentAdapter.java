package id.asteris.testandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.CheckedOutputStream;

import id.asteris.testandroid.R;
import id.asteris.testandroid.model.Comment;
import id.asteris.testandroid.model.Story;

public class CommentAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Comment> comments;

    public CommentAdapter(Context context, ArrayList comments) {
        this.context = context;
        this.comments = comments;
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Comment comment = comments.get(position);

        if(convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.list_view_comment_adapter, null);
        }

        final TextView user = convertView.findViewById(R.id.listViewComment_user);
        final TextView text = convertView.findViewById(R.id.listViewComment_text);
        final TextView date = convertView.findViewById(R.id.listViewComment_date);

        user.setText(comment.getBy());
        text.setText(comment.getText());
        date.setText(comment.getDate());
        return convertView;
    }
}
