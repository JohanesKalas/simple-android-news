package id.asteris.testandroid.model;

import android.text.Html;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class Comment {
    private int id;
    private String by;
    private int idParent;
    private int[] idChilds;
    private String text;
    private int date;

    public Comment() {}

    public Comment(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.by = jsonObject.getString("by");
            this.idParent = jsonObject.getInt("parent");
            this.text = jsonObject.getString("text");
            boolean has = jsonObject.has("kids");
            if(has) {
                JSONArray arr = jsonObject.getJSONArray("kids");
                idChilds = new int[arr.length()];
                for (int i = 0; i < arr.length(); i++) {
                    idChilds[i] = arr.getInt(i);
                }
            }
            this.date = jsonObject.getInt("time");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public int getIdParent() {
        return idParent;
    }

    public void setIdParent(int idParent) {
        this.idParent = idParent;
    }

    public int[] getIdChilds() {
        return idChilds;
    }

    public void setIdChilds(int[] idChilds) {
        this.idChilds = idChilds;
    }

    public String getText() {
        if(text != null) {
            return Html.fromHtml(text).toString();
        } else {
            return "NaN";
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        Date time = new Date((long)date*1000);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        return calendar.get(Calendar.DATE) + "/" + calendar.get(Calendar.MONTH) + "/" + calendar.get(Calendar.YEAR);
    }

    public void setDate(int date) {
        this.date = date;
    }
}
