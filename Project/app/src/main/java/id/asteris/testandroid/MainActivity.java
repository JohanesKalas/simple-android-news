package id.asteris.testandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import id.asteris.testandroid.adapter.TopStoriesAdapter;
import id.asteris.testandroid.connection.AppAPI;
import id.asteris.testandroid.connection.GetInfoTopStories;
import id.asteris.testandroid.connection.GetTopStories;
import id.asteris.testandroid.model.Story;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    private GridView gv;

    private ArrayList<Story> topStories;

    public static String favTitle = "";

    Story[] stories;

    private GetTopStories getTopStories;
    private TopStoriesAdapter adapter;

    private ProgressBar progressBar;
    private TextView lastClicked;
    int p = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        topStories = new ArrayList<>();

        gv = findViewById(R.id.aMain_gridView);
        lastClicked = findViewById(R.id.aMain_lastClicked);

        adapter = new TopStoriesAdapter(this, topStories);
        gv.setAdapter(adapter);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Story story = topStories.get(position);
                FullStoryActivity.story = story;
                Intent intent = new Intent(view.getContext(), FullStoryActivity.class);
                intent.putExtra("id", story.getId());
                startActivity(intent);
            }
        });

        getTopStories = new GetTopStories(this, AppAPI.BASE_URL + "topstories.json?print=pretty");
        getTopStories.execute();

        lastClicked.setText("My Fav: \n" + favTitle);

        progressBar = findViewById(R.id.aMain_progress);

        progressBar.setProgress(0);
    }

    public void addStory(Story story) {
        topStories.add(story);
        adapter.notifyDataSetChanged();
        p++;
        progressBar.setProgress(p);
        if(p == progressBar.getMax()) {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void setUpProgress(int max, int[] id) {
        progressBar.setMax(max);
        for(int i=0;i<id.length;i++) {
            GetInfoTopStories getInfoTopStories = new GetInfoTopStories(this, id[i]);
            getInfoTopStories.execute();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        lastClicked.setText("My Fav: \n" + favTitle);
    }
}
