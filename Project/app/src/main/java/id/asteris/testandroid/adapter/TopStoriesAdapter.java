package id.asteris.testandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import id.asteris.testandroid.R;
import id.asteris.testandroid.model.Story;

public class TopStoriesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Story> stories;

    public TopStoriesAdapter(Context context, ArrayList stories) {
        this.context = context;
        this.stories = stories;
    }

    @Override
    public int getCount() {
        return stories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Story story = stories.get(position);

        if(convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.top_story_grids, null);
        }

        final TextView title = convertView.findViewById(R.id.topStoryGrid_title);
        final TextView score = convertView.findViewById(R.id.topStoryGrid_score);
        final TextView comments = convertView.findViewById(R.id.topStoryGrid_comments);

        title.setText(story.getTitle());
        score.setText("Score: " + story.getScore());
        comments.setText("Comments: " + story.getTotalComment());

        return convertView;
    }
}
