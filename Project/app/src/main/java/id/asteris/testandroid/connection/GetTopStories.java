package id.asteris.testandroid.connection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.asteris.testandroid.MainActivity;

public class GetTopStories extends AsyncTask<Void, Void, Void> {

    public static String TAG = GetTopStories.class.getSimpleName();

    private Activity activity;
    private ProgressDialog pd;

    private String url;

    private int[] topStorieIds;
    public GetTopStories(Activity activity, String url) {
        this.activity = activity;
        pd = new ProgressDialog(activity);
        this.url = url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        HttpHandler sh = new HttpHandler();

        String jsonStr = sh.makeServiceCall(url);

        Log.d(TAG, "Response from URL: " + jsonStr);

        if(jsonStr != null) {
            try {
                JSONArray arr = new JSONArray(jsonStr);
                topStorieIds = new int[arr.length()];
                for(int i=0;i<arr.length();i++) {
                    topStorieIds[i] = arr.getInt(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(activity instanceof MainActivity) {
            ((MainActivity) activity).setUpProgress(topStorieIds.length, topStorieIds);
        }

        if(pd.isShowing()) {
            pd.dismiss();
        }
    }
}
