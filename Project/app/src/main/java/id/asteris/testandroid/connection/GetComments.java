package id.asteris.testandroid.connection;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import id.asteris.testandroid.FullStoryActivity;
import id.asteris.testandroid.MainActivity;
import id.asteris.testandroid.model.Comment;
import id.asteris.testandroid.model.Story;

public class GetComments extends AsyncTask<Void, Void, Void> {
    public static String TAG = GetComments.class.getSimpleName();

    private Comment comment;
    private Activity activity;
    private int idComment;
    private String url;

    public GetComments(Activity activity, int idComment) {
        this.activity = activity;
        this.idComment = idComment;
        this.url = AppAPI.BASE_URL + "item/" + idComment + ".json?print=pretty";
    }

    @Override
    protected Void doInBackground(Void... voids) {
        HttpHandler sh = new HttpHandler();

        String jsonStr = sh.makeServiceCall(url);

        Log.d(TAG, "Response from URL: " + jsonStr);

        if(jsonStr != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                comment = new Comment(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(activity instanceof FullStoryActivity) {
            ((FullStoryActivity) activity).addComment(comment);
        }
    }
}
