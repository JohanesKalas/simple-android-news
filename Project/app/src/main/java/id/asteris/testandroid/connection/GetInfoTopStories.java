package id.asteris.testandroid.connection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.asteris.testandroid.MainActivity;
import id.asteris.testandroid.model.Story;

public class GetInfoTopStories extends AsyncTask<Void, Void, Void> {
    public static String TAG = GetInfoTopStories.class.getSimpleName();

    private int storyId;
    private Story story;
    private Activity activity;
    private String url;

    private ProgressDialog pd;

    public GetInfoTopStories(Activity activity, int storyId) {
        this.activity = activity;
        this.storyId = storyId;
        this.url = AppAPI.BASE_URL + "item/" + storyId + ".json?print=pretty";
        this.pd = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pd.setMessage("Please wait...");
        pd.setCancelable(false);
//        pd.show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        HttpHandler sh = new HttpHandler();

        String jsonStr = sh.makeServiceCall(url);

        Log.d(TAG, "Response from URL: " + jsonStr);

        if(jsonStr != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                story = new Story(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(activity instanceof MainActivity) {
            ((MainActivity) activity).addStory(story);
        }

//        if(pd.isShowing()) pd.dismiss();
    }
}
