package id.asteris.testandroid.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class Story {
    private int id;
    private String by;
    private int totalComment;
    private int[] comments;
    private int score;
    private String title;
    private String url;
    private int date;

    public Story() {}

    public Story(int id, String by, int totalComment, int[] comments, int score, String title, String url, int date) {
        this.id = id;
        this.by = by;
        this.totalComment = totalComment;
        this.comments = comments;
        this.score = score;
        this.title = title;
        this.url = url;
        this.date = date;
    }

    public Story(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.by = jsonObject.getString("by");
            this.totalComment = jsonObject.getInt("descendants");
            boolean has = jsonObject.has("kids");
            if(has) {
                JSONArray arr = jsonObject.getJSONArray("kids");
                comments = new int[arr.length()];
                for (int i = 0; i < arr.length(); i++) {
                    comments[i] = arr.getInt(i);
                }
            }
            this.score = jsonObject.getInt("score");
            this.title = jsonObject.getString("title");
            has = jsonObject.has("url");
            if(has) {
                this.url = jsonObject.getString("url");
            }
            this.date = jsonObject.getInt("time");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public int getTotalComment() {
        return totalComment;
    }

    public void setTotalComment(int totalComment) {
        this.totalComment = totalComment;
    }

    public int[] getComments() {
        return comments;
    }

    public void setComments(int[] comments) {
        this.comments = comments;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        Date time = new Date((long)date*1000);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        return calendar.get(Calendar.DATE) + "/" + calendar.get(Calendar.MONTH) + "/" + calendar.get(Calendar.YEAR);
    }

    public void setDate(int date) {
        this.date = date;
    }
}
